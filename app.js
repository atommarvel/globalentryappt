const Moment = require('moment');
const Request = require('request');
const Promise = require('bluebird');
const Notifier = require('node-notifier');
const Opn = require('opn');


const locationPlaceholder = "{location}";
const baseURL = `https://ttp.cbp.dhs.gov/schedulerapi/locations/${locationPlaceholder}/slots`;
const config = require('./config.json');
const timeFormat = "Y-MM-DDTkk:mm";
const notifTimeFormat = "dddd, MMMM Do, h:mm a";

function buildURL() {
    let url = baseURL.replace(locationPlaceholder, config.locationId);

    let now = Moment().add("2", "hour");
    const start = `startTimestamp=${now.format(timeFormat)}`;

    let then = now.add(config.range[0], config.range[1]);
    const end = `endTimestamp=${then.format(timeFormat)}`;

    url = `${url}?${start}&${end}`;

    return url;
}

function fetchJsonData(url) {
    return new Promise((resolve, reject) => {
        Request(url, (error, response, body) => {
            if (error) {
                console.log(error);
                reject(error);
            }
            if (response && response.statusCode !== 200) {
                console.log('statusCode:', response && response.statusCode);
                reject();
            }
            resolve(JSON.parse(body));
        });
    });
}

function areAnyActiveBeforeCurrAppt(data, currAppt) {
    let result = false;
    let slot = null;
    data.some((day) => {
        if (day.active === 0) {
            return result;
        }

        slot = Moment(day.timestamp, timeFormat);
        result = currAppt.isAfter(slot);
        return result;
    });

    return {
        didFindBetter: result,
        slot: slot
    };
}

function notify(slot) {
    Notifier.notify(
        {
            title: 'Better Appointment Available!!',
            message: slot.format(notifTimeFormat),
            sound: true, // Only Notification Center or Windows Toasters
            wait: true, // Wait with callback, until user action is taken against notification
            actions: "Visit Site"
        },
        function(err, response) {
            Opn("https://ttp.cbp.dhs.gov")
        }
    );

}

async function checkForAvailability() {
    const url = buildURL();
    const data = await fetchJsonData(url);
    const currAppt = Moment(config.currAppt, timeFormat);
    const result = areAnyActiveBeforeCurrAppt(data, currAppt);
    if (result.didFindBetter) {
        notify(result.slot);
    }
}

function main() {
    const interval = 1 * 60 * 1000; // change first number (minutes)
    setInterval(checkForAvailability, interval);
}